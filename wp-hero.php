<?php
/**
 * WordPress Widget Boilerplate
 *
 * The WordPress Widget Boilerplate is an organized, maintainable boilerplate for building widgets using WordPress best practices.
 *
 * @package   WP_Hero
 * @author    Nicko Picazo <nickopicazo@gmail.com>
 * @license   GPL-2.0+
 * @link      http://nickopicazo.me
 * @copyright 2015 Nicko Picazo
 *
 * @wordpress-plugin
 * Plugin Name:       WP Hero
 * Plugin URI:        http://example.com/wp-hero-uri/
 * Description:       WP Hero is a WordPress plugin that can create multiple hero sections to showcase key content on your site.
 * Version:           0.0.1
 * Author:            Nicko Picazo
 * Author URI:        http://nickopicazo.me
 * Text Domain:       wp-hero
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /lang
 * GitHub Plugin URI: https://github.com/<owner>/<repo>
 */

 // Prevent direct file access
if ( ! defined ( 'ABSPATH' ) ) {
	exit;
}

// TODO: change 'WP_Hero' to the name of your plugin
class WP_Hero extends WP_Widget {

    /**
     * @TODO - Rename "wp-hero" to the name your your widget
     *
     * Unique identifier for your widget.
     *
     *
     * The variable name is used as the text domain when internationalizing strings
     * of text. Its value should match the Text Domain file header in the main
     * widget file.
     *
     * @since    0.0.1
     *
     * @var      string
     */
    protected $widget_slug = 'wp-hero';

	/*--------------------------------------------------*/
	/* Constructor
	/*--------------------------------------------------*/

	/**
	 * Specifies the classname and description, instantiates the widget,
	 * loads localization files, and includes necessary stylesheets and JavaScript.
	 */
	public function __construct() {

		// load plugin text domain
		add_action( 'init', array( $this, 'widget_init' ) );
		add_action( 'cmb2_init', array( $this, 'register_metabox' ) );

		// Hooks fired when the Widget is activated and deactivated
		register_activation_hook( __FILE__, array( $this, 'activate' ) );
		register_deactivation_hook( __FILE__, array( $this, 'deactivate' ) );

		// TODO: update description
		parent::__construct(
			$this->get_widget_slug(),
			__( 'WP Hero Widget', $this->get_widget_slug() ),
			array(
				'classname'  => $this->get_widget_slug().'-class',
				'description' => __( 'Short description of the widget goes here.', $this->get_widget_slug() )
			)
		);

		// Register admin styles and scripts
		add_action( 'admin_print_styles', array( $this, 'register_admin_styles' ) );
		//add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );

		// Register site styles and scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_styles' ) );
		//add_action( 'wp_enqueue_scripts', array( $this, 'register_widget_scripts' ) );

		// Refreshing the widget's cached output with each new post
		add_action( 'save_post',    array( $this, 'flush_widget_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_widget_cache' ) );
		add_action( 'switch_theme', array( $this, 'flush_widget_cache' ) );

	} // end constructor


    /**
     * Return the widget slug.
     *
     * @since    0.0.1
     *
     * @return    Plugin slug variable.
     */
    public function get_widget_slug() {
        return $this->widget_slug;
    }

	/*--------------------------------------------------*/
	/* Widget API Functions
	/*--------------------------------------------------*/

	/**
	 * Outputs the content of the widget.
	 *
	 * @param array args  The array of form elements
	 * @param array instance The current instance of the widget
	 */
	public function widget( $args, $instance ) {


		// Check if there is a cached output
		$cache = wp_cache_get( $this->get_widget_slug(), 'widget' );

		if ( !is_array( $cache ) )
			$cache = array();

		if ( ! isset ( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset ( $cache[ $args['widget_id'] ] ) )
			return print $cache[ $args['widget_id'] ];

		// go on with your widget logic, put everything into a string and …


		extract( $args, EXTR_SKIP );

		$widget_string = $before_widget;

		// TODO: Here is where you manipulate your widget's values based on their input fields
		ob_start();
		include( plugin_dir_path( __FILE__ ) . 'views/widget.php' );
		$widget_string .= ob_get_clean();
		$widget_string .= $after_widget;


		$cache[ $args['widget_id'] ] = $widget_string;

		wp_cache_set( $this->get_widget_slug(), $cache, 'widget' );

		print $widget_string;

	} // end widget


	public function flush_widget_cache()
	{
    	wp_cache_delete( $this->get_widget_slug(), 'widget' );
	}
	/**
	 * Processes the widget's options to be saved.
	 *
	 * @param array new_instance The new instance of values to be generated via the update.
	 * @param array old_instance The previous instance of values before the update.
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

    $instance['hero'] = ( ! empty( $new_instance['hero'] ) ) ? strip_tags( $new_instance['hero'] ) : '';

		return $instance;

	} // end widget

	/**
	 * Generates the administration form for the widget.
	 *
	 * @param array instance The array of keys and values for the widget.
	 */
	public function form( $instance ) {

		// TODO: Define default values for your variables
		$instance = wp_parse_args(
			(array) $instance
		);

		// TODO: Store the values of the widget in their own variable

		// Display the admin form
		include( plugin_dir_path(__FILE__) . 'views/admin.php' );

	} // end form

	/*--------------------------------------------------*/
	/* Public Functions
	/*--------------------------------------------------*/

	/**
	 * Loads the Widget's text domain for localization and translation.
	 */
	public function widget_init() {

		// TODO be sure to change 'wp-hero' to the name of *your* plugin
		load_plugin_textdomain( $this->get_widget_slug(), false, plugin_dir_path( __FILE__ ) . 'lang/' );

		$this->register_custom_post_type();

	} // end widget_textdomain

	/**
	 * Fired when the plugin is activated.
	 *
	 * @param  boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public function activate( $network_wide ) {
		// TODO define activation functionality here
	} // end activate

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @param boolean $network_wide True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog
	 */
	public function deactivate( $network_wide ) {
		// TODO define deactivation functionality here
	} // end deactivate

	/**
	 * Registers and enqueues admin-specific styles.
	 */
	public function register_admin_styles() {

		wp_enqueue_style( $this->get_widget_slug().'-admin-styles', plugins_url( 'css/admin.css', __FILE__ ) );

	} // end register_admin_styles

	/**
	 * Registers and enqueues admin-specific JavaScript.
	 */
	public function register_admin_scripts() {

		wp_enqueue_script( $this->get_widget_slug().'-admin-script', plugins_url( 'js/admin.js', __FILE__ ), array('jquery') );

	} // end register_admin_scripts

	/**
	 * Registers and enqueues widget-specific styles.
	 */
	public function register_widget_styles() {

		wp_enqueue_style( $this->get_widget_slug().'-widget-styles', plugins_url( 'css/widget.css', __FILE__ ) );

	} // end register_widget_styles

	/**
	 * Registers and enqueues widget-specific scripts.
	 */
	public function register_widget_scripts() {

		wp_enqueue_script( $this->get_widget_slug().'-script', plugins_url( 'js/widget.js', __FILE__ ), array('jquery') );

	} // end register_widget_scripts

	/**
	 * Registers cmb2 metaboxes
	 */
	public function register_metabox() {

    $prefix = '_hero_';

    $cmb = new_cmb2_box( array(
        'id'            => $prefix . 'section_settings',
        'title'         => __( 'Hero Settings', 'cmb2' ),
        'object_types'  => array( 'hero', ),
        'context'       => 'normal',
        'priority'      => 'default',
        'show_names'    => true,
        'closed'     => false,
    ) );

		$cmb->add_field( array(
		    'name'    => 'Section Content',
		    'desc'    => '',
		    'id'      => $prefix . 'section_content',
		    'type'    => 'wysiwyg',
				'options' => array(
						'wpautop' => false,
						'textarea_rows' => get_option('default_post_edit_rows', 5),
				),
		) );

		$cmb->add_field( array(
		    'name'    => 'Section Type',
		    'id'      => $prefix . 'section_type',
		    'type'    => 'radio_inline',
				'default' => 'hero',
		    'options' => array(
		        'hero'	=> __( 'Hero', 'cmb' ),
		        'cta'		=> __( 'Call to Action', 'cmb' ),
		    ),
		) );

		$cmb->add_field( array(
		    'name'    => 'Background Image',
		    'desc'    => 'Upload an image or enter an URL.',
		    'id'      => $prefix . 'bg_image',
		    'type'    => 'file',
		    // Optionally hide the text input for the url:
		    'options' => array(
		        'url' => false,
		    ),
		) );

		$cmb->add_field( array(
		    'name'    => 'Overlay Color',
		    'id'      => $prefix . 'overlay_color',
		    'type'    => 'colorpicker',
		    'default' => '#1a76c1',
		) );

		$cmb->add_field( array(
		    'name'             => 'Overlay Opacity',
		    'desc'             => '',
		    'id'               => $prefix . 'overlay_opacity',
		    'type'             => 'select',
		    'show_option_none' => true,
		    'default'          => '100',
		    'options'          => array(
		        '100' => __( '100%', 'cmb' ),
		        '90'	=> __( '90%', 'cmb' ),
						'80'	=> __( '80%', 'cmb' ),
						'75'	=> __( '75%', 'cmb' ),
						'70'	=> __( '70%', 'cmb' ),
						'60'	=> __( '60%', 'cmb' ),
						'50'	=> __( '50%', 'cmb' ),
						'40'	=> __( '40%', 'cmb' ),
						'30'	=> __( '30%', 'cmb' ),
						'25'	=> __( '25%', 'cmb' ),
						'20'	=> __( '20%', 'cmb' ),
						'10'	=> __( '10%', 'cmb' ),
						'0'		=> __( '0%', 'cmb' ),
		    ),
		) );

		$cmb->add_field( array(
		    'name'    => 'Text Color',
		    'id'      => $prefix . 'text_color',
		    'type'    => 'colorpicker',
		    'default' => '#FFFFFF',
		) );

		$cmb->add_field( array(
		    'name'    => 'Section ID',
		    'desc'    => '',
		    'default' => '',
		    'id'      => $prefix . 'hero_id',
		    'type'    => 'text_medium'
		) );

	} // end register_metabox()

	/**
	 * Registers custom post type
	 */
	public function register_custom_post_type() {

		$labels = array(
			'name'                => _x( 'Sections', 'Post Type General Name', 'wp-hero' ),
			'singular_name'       => _x( 'Section', 'Post Type Singular Name', 'wp-hero' ),
			'menu_name'           => __( 'WP Hero', 'wp-hero' ),
			'name_admin_bar'      => __( 'WP Hero', 'wp-hero' ),
			'parent_item_colon'   => __( 'Parent Section:', 'wp-hero' ),
			'all_items'           => __( 'All Sections', 'wp-hero' ),
			'add_new_item'        => __( 'Add New Section', 'wp-hero' ),
			'add_new'             => __( 'Add New Section', 'wp-hero' ),
			'new_item'            => __( 'New Section', 'wp-hero' ),
			'edit_item'           => __( 'Edit Section', 'wp-hero' ),
			'update_item'         => __( 'Update Section', 'wp-hero' ),
			'view_item'           => __( 'View Section', 'wp-hero' ),
			'search_items'        => __( 'Search Section', 'wp-hero' ),
			'not_found'           => __( 'Not found', 'wp-hero' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'wp-hero' ),
		);
		$args = array(
			'label'               => __( 'hero', 'wp-hero' ),
			'description'         => __( 'Post Type Description', 'wp-hero' ),
			'labels'              => $labels,
			'supports'            => array( 'title', ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-menu',
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'capability_type'     => array( 'hero' ),
		);
		register_post_type( 'hero', $args );

	} // end register_custom_post_type()

} // end class

add_action( 'widgets_init', create_function( '', 'register_widget("WP_Hero");' ) );
