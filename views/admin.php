<?php
$hero = ! empty( $instance['hero'] ) ? $instance['hero'] : __( '', 'wp-hero' );
$result = new WP_Query( array( 'post_type' => 'hero', 'post_status' => 'publish' ) );
$posts = $result->posts;
?>
<p>
  <label for="<?php echo $this->get_field_id( 'hero' ); ?>"><?php _e( 'Select Hero to Display:' ); ?></label>
  <select class="widefat" id="<?php echo $this->get_field_id( 'hero' ); ?>" name="<?php echo $this->get_field_name( 'hero' ); ?>">
    <option value="0">— Select —</option>
    <?php foreach($posts as $post) { ?>
      <option value="<?php echo $post->ID; ?>" <?php if($hero == $post->ID) echo 'selected'; ?>><?php echo $post->post_title; ?></option>
    <?php } ?>
  </select>
</p>
