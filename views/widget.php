<?php query_posts( array( 'post_type' => 'hero', 'p' => $instance['hero'], 'posts_per_page' => 1 ) ); ?>
<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>
    <?php
    $hero = array(
      'section_content' => '<h1>Hero World!</h1>',
      'section_type' => 'jumbotron',
      'bg_image' => '',
      'overlay_color' => '#000',
      'overlay_opacity' => '75',
      'text_color' => '#FFF',
      'hero_id' => 'hero_'.get_the_ID()
    );

    foreach($hero as $field => $value) {
      if( ! empty( get_post_meta( get_the_ID(), $field, true ) ) ) {
        $hero[$field] = get_post_meta( get_the_ID(), $field, true );
      }
    }
    $o = $hero['overlay_opacity'];
    $overlay_opacity = '-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity='.$o.')";filter:alpha(opacity='.$o.');-moz-opacity:'. $o/100 .';-khtml-opacity:'. $o/100 .';opacity:'. $o/100 .';';

    ?>
    <div id="<?php echo esc_html($hero['hero_id']); ?>" class="wp-hero">
      <div class="wp-jumbotron <?php echo $hero['section_type']; ?>" style="background-image:url(<?php echo $hero['bg_image']; ?>);">
        <div class="wp-hero-overlay" style="background-color:<?php echo $hero['overlay_color']; ?>;<?php echo $overlay_opacity; ?>"></div>
        <div class="wp-hero-container container">
          <div class="row">
            <div class="col col-sm-12">
              <div class="wp-hero-content" style="color:<?php echo $hero['text_color']; ?>">
                <?php echo apply_filters( 'the_content', $hero['section_content'] ); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endwhile; ?>
<?php endif; ?>
<?php wp_reset_query(); ?>
